# README #

Tests can be run via ```npm test``` after a ```npm install``` inside the frontend directory.

All that should be necessary is a ```docker-compose up``` and some patience.

Once docker has settled down, you can access the site at ```http://localhost:3000```