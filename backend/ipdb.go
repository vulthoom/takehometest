package main

import (
	"log"
	"net"

	"github.com/oschwald/geoip2-golang"
)

func dbRead(ipString string) (location Location) {
	db, err := geoip2.Open("db/GeoLite2-City.mmdb")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	if ipString == "" {
		return
	}

	ip := net.ParseIP(ipString)
	record, err := db.City(ip)
	if err != nil {
		location = Location{Country: "IP NOT PRESENT IN DATABASE!"}
		log.Print(err)
		return
	}

	location = Location{City: record.City.Names["en"],
		State:   record.Subdivisions[0].Names["en"],
		Country: record.Country.Names["en"]}
	return
}
