package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		setupResponse(&w, r)
		if (*r).Method == "OPTIONS" {
			return
		}
		body, _ := ioutil.ReadAll(r.Body)
		if string(body) != "" {
			var location Location = dbRead(string(body))
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusCreated)
			data, _ := json.Marshal(location)
			w.Write(data)
		} else {
			text, _ := json.Marshal(Location{Country: "Please enter an IP!"})
			w.Write(text)
		}
	})

	http.HandleFunc("/ip", func(w http.ResponseWriter, r *http.Request) {
		setupResponse(&w, r)

		resp, _ := http.Get("https://api.ipify.org")
		body, _ := ioutil.ReadAll(resp.Body)
		fmt.Fprintf(w, string(body))
	})

	http.ListenAndServe(":5050", nil)
}

func setupResponse(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}
