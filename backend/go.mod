module server

go 1.17

require github.com/oschwald/geoip2-golang v1.5.0

require (
	github.com/oschwald/maxminddb-golang v1.8.0 // indirect
	golang.org/x/sys v0.0.0-20211031064116-611d5d643895 // indirect
)
