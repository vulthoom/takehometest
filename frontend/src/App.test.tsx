import userEvent from '@testing-library/user-event';
import { render, screen } from '@testing-library/react';
import App from './App';

jest.mock('./service/http.service', () => {
  return {
    HttpService: jest.fn()
      .mockImplementation(() => {
        return {
          getIP: () =>
            expectedIp = "someIp",
          post: (ip: string) => {
            calledIp = ip;
            expectedLocation = { City: "someCity", State: "someState", Country: "someCountry" }
          }
        }
      })
  }
})

let expectedIp = "";
let calledIp = "";
let expectedLocation = {};

test('render sets currentIp', async () => {
  render(<App />);

  expect(expectedIp).toBe("someIp");
});

test('submit pulls location information from httpService', async () => {
  
  render(<App />);

  const ipBox = screen.getByAltText("ipBox")
  const submitButton = screen.getByAltText("submit")

  if(isElementInput(ipBox)){
    userEvent.type(ipBox, "someOtherIp")
    submitButton.click();
  } else {
    fail("Element not present!")
  }

  expect(expectedLocation).toEqual({ City: "someCity", State: "someState", Country: "someCountry" });
});

function isElementInput<T extends HTMLElement>(element: T): boolean {
  return element instanceof HTMLInputElement;
}
