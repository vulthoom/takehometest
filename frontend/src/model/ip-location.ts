export interface IpLocation {
    City: string;
    State: string;
    Country: string;
}