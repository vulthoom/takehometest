export class HttpService {

    post(body: string, action: Function) {
        fetch("http://localhost:5050/", {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: body.trim()
        })
            .then(response => response.json())
            .then(response => action(response))
    }

    getIP(action: Function) {
        fetch("http://localhost:5050/ip", {
            method: "GET",
            headers: { 'Content-Type': 'application/text' }
        })
            .then(response => response.text())
            .then(response => action(response))
    }
}