import React, { FormEventHandler, useEffect, useState } from 'react';
import './App.css';
import { HttpService } from './service/http.service';
import { IpLocation } from './model/ip-location';

let httpService: HttpService = new HttpService();

interface FormProps {
  onChange: FormEventHandler,
  onSubmit: FormEventHandler
}

const LocationForm = ({ onChange, onSubmit }: FormProps) => (
  <form onSubmit={onSubmit}>
    <label>Enter IP Address:
      <input alt="ipBox" onChange={onChange} />
    </label>
    <input alt="submit" type="submit" />
  </form>
)

const App = () => {
  const [location, setLocation] = useState<IpLocation>({} as IpLocation);
  const [currentIp, setCurrentIp] = useState<string>("");
  const [ip, setIp] = useState<string>("");

  const onChange = (event: React.FormEvent<HTMLInputElement>) => {
    event.preventDefault();
    setIp(event?.currentTarget?.value);
  }

  const onSubmit = (event: any) => {
    event.preventDefault();
    httpService.post(ip, (data: IpLocation) => setLocation(data))
  }

  useEffect(() => {
    httpService.getIP(setCurrentIp)
  }, [])

  return (
    <div>
      <div>Your current IP is: {currentIp}</div>
      <LocationForm onChange={onChange} onSubmit={onSubmit} />
      {location.City ? location.City + ',' : ''} {location.State} {location.Country}
    </div>
  );
}

export default App;
